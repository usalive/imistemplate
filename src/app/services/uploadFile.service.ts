import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpHeaders, HttpClient, HttpParams } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadFileService {

  public headers: HttpHeaders;
  websiteRoot = '';

  constructor(private http: HttpClient) {
  }

  importExcelSheet(selectedFile: any, asiPath, authToken: any): Observable<any> {
    const formData = new FormData();
    formData.append('OrderFile', selectedFile);
    formData.append('AsiPartyAPI', asiPath);
    formData.append('AsiToken', authToken);
    return this.http.post(`${environment.ApiBaseURL}/MediaOrder/ImportExcelSheet`, formData);
  }

}
