import { Component, OnInit } from '@angular/core';
import { ToastrService, ToastrModule } from 'ngx-toastr';
import { AppComponent } from '../../app.component';
import { UploadFileService } from '../../services/uploadFile.service';
import { environment } from '../../../environments/environment';

declare const $: any;
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'asi-uploadFile',
  templateUrl: './uploadFile.component.html',
  styleUrls: ['./uploadFile.component.css']
})
export class UploadFileComponent implements OnInit {

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  updatedBy = '';

  proofFileName = '';
  proofFileData = '';
  proofFileExtension = '';

  selectedFile = null;
  selectedFileData: any = null;
  msg = '';

  partyAndOrganizationData = [];
  isBillToContactCall = false;

  isStatusActive = false;
  uploadFileName = '';
  lblSuccessOrFailStatus = 'Imported successfully!';
  lblSuccessTotalOrder = 0;
  lblSuccessSuccess = 0;
  lblSuccessOrError = 0;
  errorExcelSheetPath = undefined;

  constructor(
    private toastr: ToastrService,
    public appComponent: AppComponent,
    private uploadFileService: UploadFileService) {
    ToastrModule.forRoot();
  }

  ngOnInit() {
    this.appComponent.isLoading = false;
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  showLoader() {
    this.appComponent.isLoading = true;
  }

  hideLoader() {
    setTimeout(() => {
      this.appComponent.isLoading = false;
    }, 500);
  }

  loadFile(e) {
    try {
      this.selectedFile = e.target.files[0];
      this.handleFile();
    } catch (error) {
      console.log(error);
    }
   
  }

  handleFile() {
    try {
      const file = this.selectedFile;
      if (file) {
        const reader = new FileReader();
        reader.onload = (e) => {
          const data = reader.result;
          this.selectedFileData = data;
          this.uploadFileName = file.name;
        };
        reader.onerror = (ex) => {
          console.log('got error in uploading file');
        };
        reader.readAsBinaryString(file);
      }
    } catch (error) {
      console.log(error);
    }

  }

  save() {
    try {
      this.showLoader();
      const asiPath = this.websiteRoot + 'api/Party';
      this.uploadFileService.importExcelSheet(this.selectedFile, asiPath, this.reqVerificationToken).subscribe(response => {
        this.hideLoader();
        if (response.StatusCode === 1) {
          const responseData = response.Data;
          if (responseData != null && responseData !== undefined && responseData !== '') {
            this.isStatusActive = true;
            const excelPath = responseData.ExcelSheetPath;
            this.errorExcelSheetPath = excelPath;
            this.lblSuccessOrFailStatus = 'Imported successfully!';
            this.lblSuccessTotalOrder = responseData.TotalImportOrder;
            this.lblSuccessSuccess = responseData.SuccessImportOrder;
            this.lblSuccessOrError = responseData.FailImportOrder;

            this.toastr.success('Data has been import successfully.', 'Success!');
          } else {
            this.lblSuccessTotalOrder = 0;
            this.lblSuccessSuccess = 0;
            this.lblSuccessOrError = 0;
          }

        } else {
          this.toastr.error(response.Message, 'Error!');
          this.lblSuccessTotalOrder = 0;
          this.lblSuccessSuccess = 0;
          this.lblSuccessOrError = 0;
        }
      }, error => {
        this.hideLoader();
        this.toastr.error(error, 'Error!');
        this.lblSuccessTotalOrder = 0;
        this.lblSuccessSuccess = 0;
        this.lblSuccessOrError = 0;
      });
    } catch (error) {
      this.hideLoader();
      console.log(error);
    }
  }

  downLoadExcelSheet() {
    try {
      const ePath = this.errorExcelSheetPath;
      if (ePath != null && ePath !== undefined && ePath !== '') {
        const staticPath = 'https://localhost:444//Upload/ImportOrders';
        const lastindexof = ePath.lastIndexOf('ImportOrders') + 12;
  
        const tmpfileName = ePath.substring(lastindexof);
        const filePath = staticPath + tmpfileName;
        try {
          window.open(filePath, '_blank');
        } catch (error) {
          this.hideLoader();
          console.log(error);
        }
      } else {
      }
    } catch (error) {
      console.log(error);
    }
  }
}
