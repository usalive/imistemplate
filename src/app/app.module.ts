
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';

// Component
import { AppComponent } from './app.component';
import { GlobalClass } from './GlobalClass';
import { UploadFileComponent } from './components/uploadFile/uploadFile.component';


// services

import { UploadFileService } from './services/uploadFile.service';

@NgModule({
  declarations: [
    AppComponent,
    UploadFileComponent
    
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot({
      preventDuplicates: true,
      timeOut: 5000,
      closeButton: true,
      positionClass: 'toast-top-right',
      progressBar: true
    }),
  ],
  providers: [ GlobalClass, UploadFileService],
  bootstrap: [AppComponent]
})
export class AppModule { }

