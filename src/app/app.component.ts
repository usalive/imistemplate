import { Component, OnInit } from '@angular/core';
import { environment } from '../environments/environment';

@Component({
  selector: 'asi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  baseUrl = '';
  websiteRoot = '';
  reqVerificationToken = '';
  imgurl = '';
  updatedBy = '';


  isLoading = true;
  ngOnInit() {
    try {
      this.baseUrl = environment.baseUrl;
      this.websiteRoot = environment.websiteRoot;
      var pathArray = this.websiteRoot.split( '/' );
      var protocol = pathArray[0];
      var host = pathArray[2];
      this.websiteRoot = protocol + '//' + host + '/';
      if (this.websiteRoot.startsWith("http://"))
      this.websiteRoot = this.websiteRoot.replace('http', 'https');
      this.reqVerificationToken = environment.token;
      this.imgurl = environment.imageUrl;
    } catch (error) {
      console.log(error);
    }
  }
}
