export const environment = {
  production: true,

  ApiBaseURL: 'https://localhost:444/api/',
  websiteRoot: JSON.parse((<HTMLInputElement>document.getElementById('__ClientContext')).value).websiteRoot,
  imageUrl: 'areas/ng/iMISAngular_Template/assets/image',
  baseUrl: JSON.parse((<HTMLInputElement>document.getElementById('__ClientContext')).value).baseUrl,
  token: (<HTMLInputElement>document.getElementById('__RequestVerificationToken')).value,
  CurrentUserName: (<HTMLInputElement>document.getElementById('ctl01_AccountArea_PartyName')).innerText
};
